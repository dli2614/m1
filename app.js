// Import Dependencies
const express = require('express');
const body_parser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
const cookie_session = require('cookie-session');

// Connect To Database
mongoose.connect('mongodb://dan:dan123@ds049104.mlab.com:49104/milestone_1', { useNewUrlParser: true })
    .then(() => console.log('MongoDB Connected...'))
    .catch(err => console.log(err));
const db = mongoose.connection;

// Create Express App
const app = express();

// Trust Proxy For Getting Client IP
app.set('trust proxy', true);

// Use Middleware
app.use(body_parser.json());
app.use(body_parser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookie_session({ keys: ['key1', 'key2'] }));

// Routes
const index_router = require('./routes/index');
const auth_router = require('./routes/auth');
const questions_router = require('./routes/questions');
app.use(index_router);
app.use(auth_router);
app.use('/questions', questions_router);

// Start Express Server
app.listen(3000, () => console.log("App listening on port 3000"));