// Import Dependencies
const express = require('express');
const router = express.Router();
const User = require('../models/User');
const Question = require('../models/Question');
const Answer = require('../models/Answer');

router.post('/add', (req, res) => {
    console.log('POST /questions/add', req.body);
    let title = req.body.title;
    let body = req.body.body;
    let tags = req.body.tags;

    // Check Args
    if (!title || !body) {
        return res.json({ status: "error", error: "Title and Body cannot be empty." });
    }

    // Check Session
    if (!req.session.user_id) {
        return res.json({ status: "error", error: "Please login to continue."})
    }

    // Query User
    User.findOne({ id: req.session.user_id }, (err, user) => {
        if (err) {
            console.log(err);
            return res.json({ status: "error", error: err });
        }
        if (!user) {
            return res.json({ status: "error", error: "User not found." });
        }
        // Add Question To Database
        let new_question = new Question({
            title: title, 
            body: body, 
            tags: tags,
            timestamp: Date.now()/1000
        });
        new_question.id = new_question._id;
        new_question.user = {
            username: user.username,
            reputation: user.reputation
        }
        new_question.save();

        // Return
        res.json({ status: "OK", id: new_question.id });
    });
});

router.get('/:id', (req, res) => {
    console.log(`GET /questions/${req.params.id}`);
    let id = req.params.id;

    // Query Question
    Question.findOne({ id: id }, (err, question) => {
        if (err) {
            console.log(err);
            return res.json({ status: "error", error: err });
        }
        if (!question) {
            return res.json({ status: "error", error: "Question not found." });
        }

        // Increment View Count
        let id = req.session.user_id;
        if (!id) {
            id = req.ip;
        }
        console.log('Client ID: ' + id);

        if (!question.viewers.includes(id)) {
            question.view_count = question.view_count + 1;
            question.viewers.push(id);
            question.save();
        }

        // Return
        res.json({ status: "OK", question: question });
    });
});

router.delete('/:id', (req, res) => {
    console.log(`DELETE /questions/${req.params.id}`);
    let id = req.params.id;

    // Check Session
    if (!req.session.user_id) {
        return res.json({ status: "error", error: "Please login to continue."})
    }

    // Query Question
    Question.findOne({ id: id }, (err, question) => {
        if (err) {
            console.log(err);
            return res.json({ status: "error", error: err });
        }
        if (!question) {
            return res.json({ status: "error", error: "Question not found." });
        }

        // Query Poster
        let username = question.user.username;
        User.findOne({ username: username }, (err, user) => {
            if (err) {
                console.log(err);
                return res.json({ status: "error", error: err });
            }
            if (!question) {
                return res.json({ status: "error", error: "User not found." });
            }

            // Check If Original Poster And Delete
            if (user.username == username) {
                Question.deleteOne({}, (err) => {
                    if (err) {
                        console.log(err);
                        return res.json({ status: "error", error: err });
                    }
                    return res.json({ status: "OK" });
                });
            } else {
                return res.json({ status: "error", error: "User not original poster." });
            }
        });
    });
});

router.post('/:id/answers/add', (req, res) => {
    console.log(`POST /questions/${req.params.id}/answers/add`);
    let id = req.params.id;
    let body = req.body.body;
    let media = req.body.media;

    // Check Session
    if (!req.session.user_id) {
        return res.json({ status: "error", error: "Please login to continue."})
    }

    // Query Question
    Question.findOne({ id: id }, (err, question) => {
        if (err) {
            console.log(err);
            return res.json({ status: "error", error: err });
        }
        if (!question) {
            return res.json({ status: "error", error: "Question not found." });
        }

        User.findOne({ id: req.session.user_id }, (err, user) => {
            // Add Answer To Database
            new_answer = new Answer({
                user: user.username, // ID of Poster
                question: question.id, // ID of Questions
                body: body,
                media: media,
                timestamp: Date.now()/1000
            });
            new_answer.id = new_answer._id;
            new_answer.save();

            // Increment Answer Count 
            question.answer_count = question.answer_count + 1;
            question.save();

            // Return
            res.json({ status: "OK", id: new_answer.id });
        });
    });
});

router.get('/:id/answers', (req, res) => {
    console.log(`GET /questions/${req.params.id}/answers`);
    let id = req.params.id;

    // Query Answers
    Answer.find({ question: id }, (err, answers) => {
        if (err) {
            console.log(err);
            return res.json({ status: "error", error: err });
        }

        res.json({ status: "OK", answers: answers });
    });
});

module.exports = router;