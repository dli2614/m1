// Import Dependencies
const express = require('express');
const path = require('path');
const router = express.Router();
const Question = require('../models/Question');


router.post('/search', (req, res) => {
    console.log('POST /search', req.body);
    let timestamp = req.body.timestamp;
    let limit = req.body.limit;

    // Check Timestamp
    if (!timestamp) {
        timestamp = Date.now()/1000;
    } else if (typeof timestamp == 'string') {
        timestamp = Date.parse(timestamp)/1000;
    }

    // Check Limit
    if (!limit) {
        limit = 25;
    } else if (limit > 100) {
        return res.json({ status: "error", error: "Max number of questions to return: 100" });
    }

    // Query Questions
    Question.find({ timestamp: { "$lte": timestamp } }, (err, questions) => {
        return res.json({ status: "OK" , questions: questions });
    }).sort({ 'timestamp': '-1' }).limit(limit);
});

module.exports = router;