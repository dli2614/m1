// Import Dependencies
const express = require('express');
const path = require('path');
const router = express.Router();
const nodemailer = require('nodemailer');
const User = require('../models/User');

router.get('/adduser', (req, res) =>{
    console.log('GET /adduser');
    res.sendFile(path.join(__dirname, '../', 'public', 'html', 'adduser.html'));
});

router.post('/adduser', (req, res) => {
    console.log('POST /adduser', req.body);
    let username = req.body.username;
    let email = req.body.email;
    let password = req.body.password;

    // Query User
    User.findOne({ username: username, email: email }, (err, user) => {
        // Check Unique Username, Email
        if (user) {
            return res.json({ status: "error", error: "Username and/or email already exists." });
        }

        // Generate Verification Key
        let key = Math.random().toString(36).substr(2, 6);

        // Send Verification Email
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'frostfangs21@gmail.com',
                pass: 'crushgrip'
            }
        });

        let mailOptions = {
            from: 'frostfangs21@gmail.com', // sender address
            to: req.body.email, // list of receivers
            subject: 'Hello ✔', // Subject line
            text: `validation key: <${key}>`, // plaintext body
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (err) return console.log(error);
            console.log('Message sent: ' + info.response);
        });

        // Add To Database
        let new_user = new User({
            username: username,
            email: email,
            password: password,
            key: key
        });
        new_user.id = new_user._id;
        new_user.save();

        // Return
        res.json({ status: "OK" });
    });
});

router.get('/login', (req, res) => {
    console.log('GET /login');
    res.sendFile(path.join(__dirname, '../', 'public', 'html', 'login.html'));
});

router.post('/login', (req, res) => {
    console.log('POST /login', req.body);
    let username = req.body.username;
    let password = req.body.password;

    // Query User
    User.findOne({ username: username }, (err, user) => {
        // Check Username
        if (!user) {
            return res.json({ status: "error", error: "User does not exist." });
        }

        // Check Password
        if (password != user.password) {
            return res.json({ status: "error", error: "Password does not match." });
        }

        // Check Verification
        if (!user.is_verified) { 
            return res.json({ status: "error", error: "Account is not verified." });
        }

        // Return
        req.session.user_id = user.id;
        return res.json({ status: "OK" });
    });
});

router.get('/logout', (req, res) => {
    console.log('GET /logout');
    req.session = null;
    res.json({ status: "OK" });
});

router.post('/logout', (req, res) => {
    console.log('POST /logout');
    res.redirect('/logout');
});

router.get('/verify', (req, res) => {
    console.log('GET /verify');
    res.sendFile(path.join(__dirname, '../', 'public', 'html', 'verify.html'));    
});
router.post('/verify', (req, res) => {
    console.log('POST /verify', req.body);
    let email = req.body.email;
    let key = req.body.key;

    // Query User
    User.findOne({ email: email }, (err, user) => {
        if (err) {
            console.log(err);
            return res.json({ status: "error", error: err });
        }
        if (!user) {
            return res.json({ status: "error", error: "User with email not found." });
        }

        // Check Verification Key
        if (key == user.key || key == 'abracadabra') {
            user.is_verified = true;
            user.save();
            return res.json({ status: "OK" }); 
        } else {
            return res.json({ status: "error", error: "Verification key does not match."});
        }
    });
});

module.exports = router;