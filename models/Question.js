const mongoose = require('mongoose');

module.exports = mongoose.model('Question', {
    id: String,
    user: {
        username: String, 
        reputation: Number
    },
    title: String,
    body: String,
    score: { type: Number, default: 0 },
    view_count: { type: Number, default: 0 },
    answer_count: { type: Number, default: 0 },
    timestamp: Number, // Seconds Since UNIX Epoch
    media: [String], // Array of Media IDs
    tags: [String],
    accepted_answer_id: String,
    viewers: [String]
});