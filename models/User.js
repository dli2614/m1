const mongoose = require('mongoose');

module.exports = mongoose.model('User', {
    id: String,
    username: String,
    email: String,
    password: String,
    is_verified: { type: Boolean, default: false },
    key: String,
    reputation: { type: Number, default: 1 }
});