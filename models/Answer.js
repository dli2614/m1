const mongoose = require('mongoose');

module.exports = mongoose.model('Answer', {
    id: String,
    user: String, // ID of Poster
    question: String, // ID of Questions
    body: String,
    score: { type: Number, default: 0 },
    is_accepted: { type: Boolean, default: false },
    timestamp: Number, // Seconds Since UNIX Epoch
    media: [String]
});